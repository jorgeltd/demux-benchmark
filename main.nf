
samples = Channel.fromPath(params.samples)
barcodes = Channel.fromPath(params.barcodes_fw) \
                    | splitCsv(header:true) \
                    | map { row-> tuple(row.Sample, row.Sequence, row.Name) }
barcode_csv = Channel.fromPath(params.barcodes_fw)
error_demux = Channel.fromList([0.05, 0.10, 0.15, 0.2, 0.25, 0.3, 0.35, 0.40, 0.45, 0.50])

workflow{
    createFasta(samples)
    resampleFasta(createFasta.out)

    resampleFasta.out
        | join(barcodes)
        | barcodeSample


    catBarcodes(barcodeSample.out.barcodes | collect)
    barcodesToFasta(barcode_csv)
    originalSamplesStats(barcodeSample.out.samples | collect)

    mergeSamples(barcodeSample.out.samples | collect)
    mergeSamples.out | combine(barcodesToFasta.out) | combine(error_demux) | set{dataset_with_error_rate_ch}

    // cutadapt dataset analysis
    cutadaptDemux(dataset_with_error_rate_ch)
    analyzeCutadapt(cutadaptDemux.out.results | combine(originalSamplesStats.out))
    catCutadaptCsv(analyzeCutadapt.out | collect)

    // porechop dataset analysis
    porechopDemux(dataset_with_error_rate_ch)
    analyzePorechop(porechopDemux.out.results | combine(originalSamplesStats.out))
    catPorechopCsv(analyzePorechop.out | collect)
}


process createFasta{
    label "python"

    input:
    path sample

    output:
    tuple val(sample_id), path("*.fasta.gz")

    script:
    sample_id = sample.baseName
    """
    csv2fasta.py -i $sample -o ${sample_id}.fasta
    pigz ${sample_id}.fasta
    """
 }


 process resampleFasta{
    publishDir "${params.output}/1-sample_fasta", mode: 'copy'

    input:
    tuple val(sample_id), path(fasta)

    output:
    tuple val(sample_id), path("*.resampled.fasta.gz")

    script:
    """
    zcat $fasta | seqkit sample -n ${params.max_sample_reads} -o ${sample_id}.resampled.fasta.gz
    """
 }


 process barcodeSample{
    publishDir "${params.output}/2-barcoded_sample/samples", mode: 'copy', pattern: "*.barcoded.fasta.gz"

    label "python"

    input:
    tuple val(sample_id), path(fasta), val(barcode), val(bc_name)


    output:
    path "*.barcoded.fasta.gz", emit: samples
    path "${sample_id}.barcode.csv", emit: barcodes

    script:
    """
    barcode_sample.py --sample-fasta $fasta --barcode $barcode --barcode-name $bc_name --output-fasta ${sample_id}.barcoded.fasta --output-stats ${sample_id}.barcode.csv
    pigz ${sample_id}.barcoded.fasta
    """
 }


process catBarcodes{
    publishDir "${params.output}/2-barcoded_sample", mode: 'copy'

    input:
    path barcodes_csv_files

    output:
    path "barcodes.csv.gz"

    script:
    """
    awk '(NR == 1) || (FNR > 1)' $barcodes_csv_files | pigz > barcodes.csv.gz
    """
}


 process originalSamplesStats{
    publishDir "${params.output}/2-barcoded_sample", mode: 'copy'

    input:
    path samples_fasta

    output:
    path "original_samples_stats.tsv"

    script:
    """
    seqkit stats -T $samples_fasta > original_samples_stats.tsv
    find original_samples_stats.tsv -type f -exec sed -i 's/.barcoded.fasta.gz//g' {} \\;
    """

}


process barcodesToFasta{
    publishDir "${params.output}/3-barcodes", mode: 'copy'
    label "python"

    input:
    path barcodes

    output:
    path "barcodes.fasta"

    script:
    """
    barcodes2fasta.py --input $barcodes --output barcodes.fasta
    """
}


process mergeSamples{
    publishDir "${params.output}/4-work_sample", mode: 'copy'

    input:
    path samples

    output:
    path "merged_samples.fasta.gz"

    script:
    """
    cat $samples > merged_samples.fasta.gz
    """
}


process cutadaptDemux{
    publishDir "${params.output}/5-cutadapt_demux/${error_rate}", mode: 'copy'
    label "cutadapt"
    tag "${error_rate}"

    input:
    tuple path(merged_fasta), path(barcodes_fasta), val(error_rate)

    output:
    path "error_rate.txt"
    path "unknown.fasta.gz", emit: unknown
    path "*.pass.fasta.gz", emit: samples

    tuple val(error_rate), path("*.pass.fasta.gz"), path("unknown.fasta.gz"), emit: results

    script:
    """
    cutadapt -e $error_rate --cores=${task.cpus} --untrimmed-output unknown.fasta.gz \
           -g file:${barcodes_fasta} -o "{name}.pass.fasta.gz" $merged_fasta
    echo $error_rate > error_rate.txt
    """
}


process analyzeCutadapt{
    //publishDir "${params.output}/6-analyze_cutadapt", mode: 'copy'
    label "python"

    input:
    tuple val(error_rate), path(samples), path(unknown_reads), path(samples_stats)

    output:
    path "*.csv"

    script:
    """
    analyse_experiment.py $samples --unknown-reads $unknown_reads \
    --samples-stats $samples_stats --error-rate $error_rate --output cutadapt_${error_rate}.csv
    """
}


process catCutadaptCsv{
    publishDir "${params.output}/6-analyze_cutadapt", mode: 'copy'

    input:
    path cutadapt_csv_files

    output:
    path "cutadapt_results.csv"

    script:
    """
    awk '(NR == 1) || (FNR > 1)' $cutadapt_csv_files > cutadapt_results.csv
    """
}


process porechopDemux{
    publishDir "${params.output}/7-porechop_demux/${error_rate}", mode: 'copy'
    tag "${error_rate}"
    label "porechop"
    cpus 5
    memory "15GB"

    input:
    tuple path(merged_fasta), path(barcodes_fasta), val(error_rate)

    output:
    path "Barcode*.fasta.gz", emit: samples
    tuple val(error_rate), path("Barcode*.fasta.gz"), path("none.fasta.gz"), emit: results


    script:
    threshold = ((1 - error_rate)*100).intValue() 
    """
    porechop-${params.porechop}.py -t ${task.cpus}  --check_reads 100000 --adapter_threshold 70 --barcode_threshold $threshold --barcode_diff 1 \
                        -i $merged_fasta -b .
    """
}


process analyzePorechop{
    //publishDir "${params.output}/6-analyze_cutadapt", mode: 'copy'
    label "python"

    input:
    tuple val(error_rate), path(samples), path(unknown_reads), path(samples_stats)

    output:
    path "*.csv"

    script:
    """
    analyse_experiment.py $samples --unknown-reads $unknown_reads \
    --samples-stats $samples_stats --error-rate $error_rate --output porechop_${error_rate}.csv
    """
}

process catPorechopCsv{
    publishDir "${params.output}/8-analyze_porechop", mode: 'copy'

    input:
    path cutadapt_csv_files

    output:
    path "porechop_results.csv"

    script:
    """
    awk '(NR == 1) || (FNR > 1)' $cutadapt_csv_files > porechop_results.csv
    """
}
