#!/usr/bin/env python
import gzip
import re
from os import path
from pathlib import Path

import pandas
import pandas as pd
import typer
from Bio import SeqIO
from typing_extensions import Annotated


def main(
    samples_paths: list[Path],
    unknown_reads: Annotated[Path, typer.Option()],
    error_rate: Annotated[float, typer.Option()],
    samples_stats: Annotated[Path, typer.Option()],
    output: Annotated[Path, typer.Option()] = "output.csv",
):
    unknown_reads_file = gzip.open(unknown_reads, "rt")
    unknown_reads_content = unknown_reads_file.read()

    stats_df = pd.read_csv(samples_stats, sep="\t")

    records = []
    for sample_path in samples_paths:
        sample_file = (
            path.basename(sample_path)
            .split(".")[0]
            .replace("Barcode_", "")
            .replace("_end", "")
            .replace("_start", "")
        )

        classified_reads = 0
        miss_classified_reads = 0
        unclassified_reads = unknown_reads_content.count(f"sample={sample_file}")
        original_sample_count = stats_df[stats_df["file"] == sample_file][
            "num_seqs"
        ].values[0]

        with gzip.open(sample_path, "rt") as handle:
            for record in SeqIO.parse(handle, "fasta"):
                sample_read = re.findall(r"sample=(\w+)", record.description)[0]

                if sample_read == sample_file:
                    classified_reads += 1
                else:
                    miss_classified_reads += 1

        records.append(
            {
                "sample": sample_file,
                "classified": classified_reads,
                "miss_classified": miss_classified_reads,
                "unclassified": unclassified_reads,
                "sample_og_size": original_sample_count,
                "error_rate": error_rate,
            }
        )

    unknown_reads_file.close()

    df = pd.DataFrame(records)
    df["total_recovery"] = df["classified"] + df["miss_classified"]
    df["recovery_success_rate"] = df["classified"] / df["total_recovery"]
    df["recovery_error_rate"] = df["miss_classified"] / df["total_recovery"]
    df.to_csv(output, index=False, float_format="%.2f")


if __name__ == "__main__":
    typer.run(main)
