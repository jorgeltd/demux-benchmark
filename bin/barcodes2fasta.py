#!/usr/bin/env python

from pathlib import Path

import pandas as pd
import typer
from typing_extensions import Annotated


def main(
    input_csv: Annotated[Path, typer.Option("--input", "-i")],
    output_fasta: Annotated[Path, typer.Option("--output", "-o")],
):
    df = pd.read_csv(input_csv, sep=",")
    with open(output_fasta, "w") as f:
        for idx, row in df.iterrows():
            f.write(f">{row['Sample']}\n")
            f.write(f"{row['Sequence']}\n")


if __name__ == "__main__":
    typer.run(main)
