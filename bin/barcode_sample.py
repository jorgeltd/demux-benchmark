#!/usr/bin/env python

import gzip
from itertools import groupby
from os import path
from pathlib import Path
from typing import Annotated

import numpy as np
import pandas as pd
import typer
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from thefuzz import fuzz

# substitution frequency ref 10.1371/journal.pone.0257521

BASE_SUB_PROB = {
    "A": {"C": 0.05, "G": 0.225, "T": 0.06, "A": 0.665},
    "C": {"A": 0.06, "G": 0.037, "T": 0.09, "C": 0.813},
    "G": {"A": 0.225, "C": 0.025, "T": 0.05, "G": 0.7},
    "T": {"A": 0.06, "C": 0.075, "G": 0.05, "T": 0.815},
}

# homopolymer error-rate ref 10.1371/journal.pone.0257521
HOM_ERROR_RATE = {
    2: 0.1,
    3: 0.15,
    4: 0.25,
    5: 0.35,
    6: 0.5,
    7: 0.6,
    8: 0.75,
    9: 0.8,
}


def del_homopolymer_bases(base: str, count: int) -> str:
    if np.random.binomial(1, HOM_ERROR_RATE[count]) == 1:
        # a error ocurred case

        extra_bases_del = int(
            np.sum([np.random.binomial(1, 0.4) for _ in range(count - 2)])
        )  # term to delete more than 1 base

        return base * (count - 1 - extra_bases_del)

    else:
        return base * count


def del_homopolymers(seq: str) -> tuple[str, str]:
    """
    Mutate a barcode sequence
    """

    mutated_seq = ""
    # mask for mutations applied to seq
    mutation_mask = ""

    # groupby seq by countinuos repeats of the same base
    for base, repeats in groupby(seq):
        repeat_len = len(list(repeats))

        if repeat_len > 1:
            mutated_base = del_homopolymer_bases(base, repeat_len)
            mutated_seq += mutated_base

            mutation_mask += (
                "d" * len(mutated_base)
                if len(mutated_base) != repeat_len
                else "-" * repeat_len
            )

        else:
            mutated_seq += base
            mutation_mask += "-"

    return mutated_seq, mutation_mask


def substitute_base(base: str) -> str:
    """
    Substitute a base by another base
    """
    base_prob = list(BASE_SUB_PROB[base].values())
    bases_list = list(BASE_SUB_PROB[base].keys())

    base_index = np.where(np.random.multinomial(1, base_prob) == 1)[0][0]

    return bases_list[base_index]


def mutate_sequence_by_substitution(seq: str, mutations_maks: str) -> tuple[str, str]:
    """
    Mutate a barcode sequence by subsitutions
    """
    mutated_seq = ""
    new_mutation_mask = ""
    for base, mutation_type in zip(seq, mutations_maks):
        if mutation_type == "-":
            new_base = substitute_base(base)
            mutated_seq += new_base
            new_mutation_mask += "s" if new_base != base else "-"
        else:
            mutated_seq += base
            new_mutation_mask += mutation_type

    return mutated_seq, new_mutation_mask


def mutate_seq(seq: str) -> str:
    """
    Mutate a barcode sequence
    """

    homopolymers_mutated, mutation_mask = del_homopolymers(seq)
    bases_mutated, mutation_mask = mutate_sequence_by_substitution(
        homopolymers_mutated, mutation_mask
    )

    return bases_mutated, mutation_mask


def gen_adapter_barcode(barcode: str) -> str:
    """
    Generate a adapter+barcode with errors
    :param barcode:
    :return:
    """

    adapter = "GTTTTCGCATTTATCGTGAAACGCTTTCGCGTTTTTCGTGCGCCGCTTCA"

    mutated_barcode, barcode_mask = mutate_seq(barcode)
    error = fuzz.ratio(mutated_barcode, barcode) / 100

    mutated_adapter, adapter_mask = mutate_seq(adapter)

    adapter_barcode = mutated_adapter + mutated_barcode

    return adapter_barcode, error


def main(
    sample_fasta: Annotated[Path, typer.Option()],
    barcode: Annotated[str, typer.Option()],
    barcode_name: Annotated[str, typer.Option()] = "BC01",
    output_fasta: Annotated[Path, typer.Option()] = "sample.barcoded.fasta",
    output_stats: Annotated[Path, typer.Option()] = "sample.barcoded.csv",
):
    sequences = []
    error_stats = []
    sample = path.basename(sample_fasta).split(".")[0]

    with gzip.open(sample_fasta, "rt") as handle, open(
        output_fasta, "w"
    ) as output_fasta:
        for record in SeqIO.parse(handle, "fasta"):
            adapter_barcode, error = gen_adapter_barcode(barcode)

            error_stats.append(
                {
                    "sample": sample,
                    "barcode": barcode,
                    "mutated_barcode": adapter_barcode,
                    "identity": error,
                }
            )
            output_fasta.write(f">{record.description} bc={barcode_name}\n")
            output_fasta.write(f"{adapter_barcode}{str(record.seq)}\n")

    error_stats_df = pd.DataFrame(error_stats)
    error_stats_df.to_csv(output_stats, index=False, float_format="%.2f")


if __name__ == "__main__":
    typer.run(main)
