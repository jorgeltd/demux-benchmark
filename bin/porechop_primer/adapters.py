"""
Copyright 2017 Ryan Wick (rrwick@gmail.com)
https://github.com/rrwick/Porechop

This module contains the class and sequences for known adapters used in Oxford Nanopore library
preparation kits.

This file is part of Porechop. Porechop is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version. Porechop is distributed in
the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details. You should have received a copy of the GNU General Public License along with Porechop. If
not, see <http://www.gnu.org/licenses/>.
"""


class Adapter(object):
    def __init__(
        self, name, start_sequence=None, end_sequence=None, both_ends_sequence=None
    ):
        self.name = name
        self.start_sequence = start_sequence if start_sequence else []
        self.end_sequence = end_sequence if end_sequence else []
        if both_ends_sequence:
            self.start_sequence = both_ends_sequence
            self.end_sequence = both_ends_sequence
        self.best_start_score, self.best_end_score = 0.0, 0.0

    def best_start_or_end_score(self):
        return max(self.best_start_score, self.best_end_score)

    def is_barcode(self):
        return self.name.startswith("Barcode ")

    def barcode_direction(self):
        if "_rev" in self.start_sequence[0]:
            return "reverse"
        else:
            return "forward"

    def get_barcode_name(self):
        """
        Gets the barcode name for the output files. We want a concise name, so it looks at all
        options and chooses the shortest.
        """
        possible_names = [self.name]
        if self.start_sequence:
            possible_names.append(self.start_sequence[0])
        if self.end_sequence:
            possible_names.append(self.end_sequence[0])
        barcode_name = sorted(possible_names, key=lambda x: len(x))[0]
        return barcode_name.replace(" ", "_")


# INSTRUCTIONS FOR ADDING CUSTOM ADAPTERS
# ---------------------------------------
# If you need Porechop to remove adapters that aren't included, you can add your own my modifying
# the ADAPTERS list below.
#
# Here is the format for a normal adapter:
#     Adapter('Adapter_set_name',
#             start_sequence=('Start_adapter_name', 'AAAACCCCGGGGTTTTAAAACCCCGGGGTTTT'),
#             end_sequence=('End_adapter_name', 'AACCGGTTAACCGGTTAACCGGTTAACCGGTT'))
#
# You can exclude start_sequence and end_sequence as appropriate.
#
# If you have custom Barcodes, make sure that the adapter set name starts with 'Barcode '. Also,
# remove the existing barcode sequences from this file to avoid conflicts:
#     Adapter('Barcode 1',
#             start_sequence=('Barcode_1_start', 'AAAAAAAACCCCCCCCGGGGGGGGTTTTTTTT'),
#             end_sequence=('Barcode_1_end', 'AAAAAAAACCCCCCCCGGGGGGGGTTTTTTTT')),
#     Adapter('Barcode 2',
#             start_sequence=('Barcode_2_start', 'TTTTTTTTGGGGGGGGCCCCCCCCAAAAAAAA'),
#             end_sequence=('Barcode_2_end', 'TTTTTTTTGGGGGGGGCCCCCCCCAAAAAAAA'))


ADAPTERS = [
    # Some barcoding kits (like the native barcodes) use the rev comp barcode at the start
    # of the read and the forward barcode at the end of the read.
    Adapter(
        "Barcode SRR1964787_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR1964787_Heavy_IGHG_start",
            "GGTAGGCGCTCTGTGTGCAGCCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR1964787_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGGCTGCACACAGAGCGCCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR3099105_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR3099105_Heavy_IGHG_start",
            "GGTAGTCATGAGTCGACACTACTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR3099105_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGTAGTGTCGACTCATGACTACC",
        ),
    ),
    Adapter(
        "Barcode SRR3620068_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR3620068_Heavy_IGHG_start",
            "GGTAGTATCTATCGTATACGCCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR3620068_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGGCGTATACGATAGATACTACC",
        ),
    ),
    Adapter(
        "Barcode SRR5811762_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR5811762_1_Heavy_IGHG_start",
            "GGTAGATCACACTGCATCTGACTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR5811762_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGTCAGATGCAGTGTGATCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR5811771_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR5811771_1_Heavy_IGHG_start",
            "GGTAGACGTACGCTCGTCATACTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR5811771_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGTATGACGAGCGTACGTCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR5811783_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR5811783_1_Heavy_IGHG_start",
            "GGTAGTGTGAGTCAGTACGCGCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR5811783_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGCGCGTACTGACTCACACTACC",
        ),
    ),
    Adapter(
        "Barcode SRR8321521_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR8321521_1_Heavy_IGHG_start",
            "GGTAGAGAGACACGATACTCACTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR8321521_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGTGAGTATCGTGTCTCTCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR8980653_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR8980653_Heavy_IGHG_start",
            "GGTAGCTGCTAGAGTCTACAGCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR8980653_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGCTGTAGACTCTAGCAGCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR12190255_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR12190255_Heavy_IGHG_start",
            "GGTAGAGCACTCGCGTCAGTGCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR12190255_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGCACTGACGCGAGTGCTCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR12326714_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR12326714_1_Heavy_IGHG_start",
            "GGTAGTCATGCACGTCTCGCTCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR12326714_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGAGCGAGACGTGCATGACTACC",
        ),
    ),
    Adapter(
        "Barcode SRR12326728_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR12326728_1_Heavy_IGHG_start",
            "GGTAGAGAGCATCTCTGTACTCTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR12326728_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGAGTACAGAGATGCTCTCTACC",
        ),
    ),
    Adapter(
        "Barcode SRR12326738_1_Heavy_IGHG (forward)",
        start_sequence=(
            "Barcode_SRR12326738_1_Heavy_IGHG_start",
            "GGTAGCGCATCGACTACGCTACTTAAGCAGTGGTATCAACGCAGAGTACG",
        ),
        end_sequence=(
            "Barcode_SRR12326738_1_Heavy_IGHG_end",
            "CGTACTCTGCGTTGATACCACTGCTTAAGTAGCGTAGTCGATGCGCTACC",
        ),
    ),
]


def make_full_native_barcode_adapter(barcode_num):
    barcode = [
        x for x in ADAPTERS if x.name == "Barcode " + str(barcode_num) + " (reverse)"
    ][0]
    start_barcode_seq = barcode.start_sequence[1]
    end_barcode_seq = barcode.end_sequence[1]

    start_full_seq = start_barcode_seq
    end_full_seq = end_barcode_seq

    return Adapter(
        "Native barcoding " + str(barcode_num) + " (full sequence)",
        start_sequence=("NB" + "%02d" % barcode_num + "_start", start_full_seq),
        end_sequence=("NB" + "%02d" % barcode_num + "_end", end_full_seq),
    )


def make_old_full_rapid_barcode_adapter(barcode_num):  # applies to SQK-RBK001
    barcode = [
        x for x in ADAPTERS if x.name == "Barcode " + str(barcode_num) + " (forward)"
    ][0]
    start_barcode_seq = barcode.start_sequence[1]

    start_full_seq = start_barcode_seq

    return Adapter(
        "Rapid barcoding " + str(barcode_num) + " (full sequence, old)",
        start_sequence=("RB" + "%02d" % barcode_num + "_full", start_full_seq),
    )


def make_new_full_rapid_barcode_adapter(barcode_num):  # applies to SQK-RBK004
    barcode = [
        x for x in ADAPTERS if x.name == "Barcode " + str(barcode_num) + " (forward)"
    ][0]
    start_barcode_seq = barcode.start_sequence[1]

    start_full_seq = start_barcode_seq

    return Adapter(
        "Rapid barcoding " + str(barcode_num) + " (full sequence, new)",
        start_sequence=("RB" + "%02d" % barcode_num + "_full", start_full_seq),
    )
