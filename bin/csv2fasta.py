#!/usr/bin/env python

from os import path
from pathlib import Path

import pandas as pd
import typer
from typing_extensions import Annotated


def main(
    input_csv: Annotated[Path, typer.Option("--input", "-i")],
    output_fasta: Annotated[Path, typer.Option("--output", "-o")],
):
    df = pd.read_csv(input_csv, sep=",", skiprows=1)
    sample = path.basename(input_csv).split(".")[0]
    with open(output_fasta, "w") as f:
        for idx, row in df.iterrows():
            f.write(f">{idx}-{sample} sample={sample} id={idx}\n")
            f.write(f"{row['sequence']}\n")


if __name__ == "__main__":
    typer.run(main)
