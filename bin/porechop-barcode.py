#!/usr/bin/env python3
"""
Convenience wrapper for running Porechop directly from source tree.
"""

from porechop_barcode.porechop import main
 
if __name__ == '__main__':
    main()
